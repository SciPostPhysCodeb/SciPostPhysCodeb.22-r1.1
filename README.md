# Codebase release 1.1 for TASEPy

by Luca Ciandrini, Richmond L. Crisostomo, Juraj Szavits-Nossan

SciPost Phys. Codebases 22-r1.1 (2023) - published 2023-12-13

[DOI:10.21468/SciPostPhysCodeb.22-r1.1](https://doi.org/10.21468/SciPostPhysCodeb.22-r1.1)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.22-r1.1) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Luca Ciandrini, Richmond L. Crisostomo, Juraj Szavits-Nossan.

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.22-r1.1](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.22-r1.1)
* Live (external) repository at [https://github.com/ExclusionProcess-tools/TASEPy/releases/tag/v1.1](https://github.com/ExclusionProcess-tools/TASEPy/releases/tag/v1.1)
